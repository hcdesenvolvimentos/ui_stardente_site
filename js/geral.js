(function(){

	$(document).ready(function(){

		$('.carrossel-destaque').owlCarousel({
			items : 1,
			dots: true,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: false,
			margin: 0,
		});

		$('.carrossel-imagem').owlCarousel({
			items : 1,
			dots: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			center: false,
			margin: 0,
			animateOut: 'fadeOut',
			autoplay: true,
			autoplayTimeout: 2500,
		});

		$('.carrossel-depoimentos').owlCarousel({
			items : 4,
			dots: true,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: true,
			margin: 45,
			responsiveClass:true,			    
			responsive:{
				320:{
					items:1,
					margin: 15,
				},
				425:{
					items:2,
					margin: 15,
				},
				500:{
					items:2,
					margin: 15,
				},
				768:{
					items:3,
					margin: 30,
				},
				850:{
					items:4,
				},
			}
		});

		$('.carrossel-time').owlCarousel({
			items : 1,
			dots: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: false,
			margin: 0,
			autoplay: true,
			autoplayTimeout: 2500,
			// responsiveClass:true,			    
			// responsive:{
			// 	320:{
			// 		items:1,

			// 	},
			// 	425:{
			// 		items:3,

			// 	},
			// 	768:{
			// 		items:4,

			// 	},
			// 	991:{
			// 		items:5,

			// 	},
			// }
		});


		// $('.carousel').carousel();

		let isClicked = false;
		$('header .navbar .navbar-toggler').click(function(){
			if(!isClicked){
				$('header .navbar .navbar-toggler .navbar-toggler-icon img').attr('src', 'img/x.svg');
				isClicked = true;
			} else{
				$('header .navbar .navbar-toggler .navbar-toggler-icon img').attr('src', 'img/menu.svg');
				isClicked = false;
			}
		});

	});

}());